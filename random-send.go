package goperf

import (
	"crypto/rand"
	"encoding/binary"
	"flag"
	"gitee.com/ha666/logs"
	"io"
	"net"
	"sync"
	"time"
)

var (
	//randomDest = flag.Bool("r", false, "Randomly connect to the server,default false.")
	randomSpan = flag.Int("span", 30, "The interval between random connections to the server,default 30 second,unit:second")
)

//目标端随机，定期间隔吞吐量测试
func SendIO() {
	const BufSize = 128 * 1024
	var wg sync.WaitGroup

	startTime := time.Now().Unix()
	for i := 0; i < *fConcurrency; i++ {
		wg.Add(1)
		go func() {
			buf := make([]byte, BufSize)
			rand.Read(buf)
			var conn net.Conn
			var err error
			for {
				conn, err = getRandomConn()
				if err != nil {
					logs.Error(err)
					continue
				} else {
					break
				}
			}

			var connWg sync.WaitGroup
			connWg.Add(2)
			go func() {
				totalBytes := int64(*fAmount) * 1024
				for totalBytes > 0 {
					_, err := conn.Write(buf)
					if err != nil {
						if err == io.EOF {
							logs.Error(err)
							break
						} else {
							logs.Error(err)
							break
						}
					}
					totalBytes -= BufSize
				}
				connWg.Done()
			}()
			go func() {
				totalBytes := int64(*fAmount) * 1024
				for {
					var count uint64
					if err := binary.Read(conn, binary.BigEndian, &count); err != nil {
						if err == io.EOF {
							logs.Error(err)
							break
						} else {
							logs.Error(err)
							break
						}
					}
					if count >= uint64(totalBytes) {
						break
					}
				}
				connWg.Done()
			}()
			connWg.Wait()
			conn.Close()
			wg.Done()
		}()
	}
	wg.Wait()

	endTime := time.Now().Unix()
	elapsed := endTime - startTime
	if elapsed == 0 {
		logs.Info("Finished in 0 second. Too fast for benchmark.")
		return
	}
	dataAmount := uint64(*fConcurrency) * uint64(*fAmount)

	speed := dataAmount / uint64(elapsed)
	logs.Info("This stage send:", dataAmount, "GB of data sent through", *fConcurrency, "connections in", elapsed, "seconds, with speed", speed, "KB/s.")
}
