package goperf

import (
	"encoding/binary"
	"flag"
	"gitee.com/ha666/logs"
	"io"
	"net"
)

var (
	flocalPort = flag.Int("port", 10001, "Port to listen on.")
	flocalIp   = flag.String("ip", "127.0.0.1", "IP to listen on.")
	keepAlive  = flag.Bool("k", false, "is keep alive")
	testType   = flag.Int("type", 0, "test type,0:concurrency,1:IOPS,2:Randomly IOPS forever,3:short link per five second,4:long link forever.")
	buffer     = flag.Int64("b", 500, "send bytes,unit: B.")
)

func receive(conn net.Conn) {
	if !*keepAlive {
		defer conn.Close()
	}
	size := int64(256 * 1024)
	if *testType == 1 {
		size = *buffer
	}
	if *testType == 0 {
		size = 256 * 1024
	}
	buf := make([]byte, size)
	var total uint64
	for {
		n, err := conn.Read(buf)
		total += uint64(n)
		if err != nil {
			if err == io.EOF {
				logs.Error("Connection finishes with", total, "bytes:", err)
				return
			} else {
				logs.Error(err)
				return
			}

		}
		if err := binary.Write(conn, binary.BigEndian, total); err != nil {
			if err == io.EOF {
				logs.Error(err)
				return
			} else {
				logs.Error(err)
				break
			}
		}
	}
}

func accept(listener net.Listener) {
	for {
		conn, err := listener.Accept()
		if err != nil {
			logs.Error(err)
			break
		}
		go receive(conn)
	}
}

func Server() {
	listener, err := net.ListenTCP("tcp4", &net.TCPAddr{
		IP:   net.ParseIP(*flocalIp),
		Port: *flocalPort,
	})
	if err != nil {
		logs.Error(err)
	}
	go accept(listener)
}
