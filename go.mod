module gitee.com/gbat/goperf

go 1.18

require (
	gitee.com/ha666/logs v2019.425.1401+incompatible
	golang.org/x/net v0.19.0
)

require gitee.com/ha666/golibs v2019.420.1435+incompatible // indirect
