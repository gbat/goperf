#!/bin/bash

start() {
  # 检查应用程序是否已经在运行
  if pgrep -f goperf_arm64 >/dev/null; then
    echo "应用程序已经在运行"
  else
    # 启动应用程序
    nohup ./goperf_arm64 &
    echo "应用程序已启动"
  fi
}

stop() {
	# 指定要查找和杀死的进程名
	process_name="goperf_arm64"

	# 使用 pgrep 命令查找匹配进程名的多个pid
	pids=$(pgrep "$process_name")

	if [ -z "$pids" ]; then
   echo "未找到进程 $process_name"
	else
   echo "找到以下进程 $process_name 的pid: $pids"

   # 使用 kill 命令逐个杀死进程
   for pid in $pids; do
  kill "$pid"
  echo "已杀死进程 $pid"
   done
	fi

	# 指定要查找和杀死的进程名
	process_name="blade"

	# 使用 pgrep 命令查找匹配进程名的多个pid
	pids=$(pgrep "$process_name")

	if [ -z "$pids" ]; then
   echo "未找到进程 $process_name"
	else
   echo "找到以下进程 $process_name 的pid: $pids"

   # 使用 kill 命令逐个杀死进程
   for pid in $pids; do
  kill "$pid"
  echo "已杀死进程 $pid"
   done
	fi
}

case "$1" in
  start)
    start
    ;;
  stop)
    stop
    ;;
  restart)
    stop
    sleep 1
    start
    ;;
  *)
    echo "使用方法: $0 {start|stop|restart}"
    exit 1
esac

exit 0